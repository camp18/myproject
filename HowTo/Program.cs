﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HowTo
{
    class Program
    {
        static void Main(string[] args)
        {
            ToArrayVsToList();
        }

        private static void ToArrayVsToList()
        {
            IEnumerable<int> range = Enumerable.Range(1, 9);

            int[] array = range.ToArray();
            Console.WriteLine($"array length is {array.Length}");

            List<int> list = range.ToList();
            Console.WriteLine($"list count is {list.Count}");
            Console.WriteLine($"list capacity is {list.Capacity}");
        }
    }
}
