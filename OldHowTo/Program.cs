﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldHowTo
{
    class Program
    {
        static void Main(string[] args)
        {
            ToArrayVsToList();
        }

        private static void ToArrayVsToList()
        {
            IEnumerable<int> range = Enumerable.Range(1, 255);

            int[] array = range.ToArray();
            Console.WriteLine($"array length is {array.Length}");

            List<int> list = range.ToList();
            Console.WriteLine($"list count is {list.Count}");
            Console.WriteLine($"list capacity is {list.Capacity}");
        }
    }
}
