﻿using Microsoft.EntityFrameworkCore;
using MyProject.Domain.Models;

namespace MyProject.Domain
{
    public sealed class MyAppContext : DbContext
    {
        public MyAppContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
    }
}
