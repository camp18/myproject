﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography;

namespace MyProject.Api.Config.Auth
{
    public class ConfigureJwtBearerOptions : IConfigureNamedOptions<JwtBearerOptions>
    {
        public ConfigureJwtBearerOptions(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(string name, JwtBearerOptions options)
        {
            IConfigurationSection sensitiveData = _configuration.GetSection("SensitiveData");
            string rsaPrivateKeyBase64 = sensitiveData.GetValue<string>("RsaPrivateKeyBase64");

            RSA rsa = RSA.Create();
            byte[] rawKey = Convert.FromBase64String(rsaPrivateKeyBase64);
            rsa.ImportRSAPrivateKey(rawKey, out _);

            options.IncludeErrorDetails = true;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new RsaSecurityKey(rsa),
                ValidateIssuer = true,
                ValidIssuer = "MyProject.Api",
                ValidateAudience = true,
                ValidAudience = "MyProject.Api",
                CryptoProviderFactory = new CryptoProviderFactory()
                {
                    CacheSignatureProviders = false
                }
            };
        }

        public void Configure(JwtBearerOptions options)
        {
            throw new NotImplementedException();
        }

        private readonly IConfiguration _configuration;
    }
}
