﻿using Microsoft.IdentityModel.Tokens;
using MyProject.Domain.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace MyProject.Api.Config.Auth
{
    public sealed class JwtGenerator
    {
        public JwtGenerator(string rsaPrivateKeyBase64)
        {
            RSA rsa = RSA.Create();
            byte[] rawKey = Convert.FromBase64String(rsaPrivateKeyBase64);
            rsa.ImportRSAPrivateKey(rawKey, out _);
            _privateRsaKey = new RsaSecurityKey(rsa);
        }

        public string CreateUserAuthToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = "MyProject.Api",
                Issuer = "MyProject.Api",
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Sid, user.Login),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, $"{user.FirstName} {user.LastName}")
                }),
                Expires = DateTime.UtcNow.AddMinutes(60),
                SigningCredentials = new SigningCredentials(_privateRsaKey, SecurityAlgorithms.RsaSha256)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = tokenHandler.WriteToken(token);
            return jwtToken;
        }

        private readonly RsaSecurityKey _privateRsaKey;
    }
}
