﻿using AutoMapper;
using MyProject.Api.Models.Dtos;
using MyProject.Api.Models.Users.Args;
using MyProject.Domain.Models;

namespace MyProject.Api.Config.Automapper
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<RegisterArgs, User>();
            CreateMap<User, UserDto>();
        }
    }
}
