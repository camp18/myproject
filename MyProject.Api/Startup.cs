using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MyProject.Api.Config.Auth;
using MyProject.Domain;
using MyProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyProject.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MyAppContext>(ops =>
            {
                var cs = Configuration.GetConnectionString("MyAppDatabase");
                ops.UseSqlServer(cs);
            });

            services.AddSingleton<IMapper>((sp) =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddMaps(Assembly.GetAssembly(typeof(Startup)));
                });
                var mapper = config.CreateMapper();
                return mapper;
            });

            services.AddSingleton<JwtGenerator>((sp) =>
            {
                // !!! DO NOT STORE ANY SENSITIVE DATA IN CONFIG FILES !!!
                // That's ONLY for demo purpose
                var sensitives = Configuration.GetSection("SensitiveData");
                var rsaPrivateKeyBase64 = sensitives.GetValue<string>("RsaPrivateKeyBase64");
                var jwtGenerator = new JwtGenerator(rsaPrivateKeyBase64);
                return jwtGenerator;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer();

            services.AddTransient<IConfigureOptions<JwtBearerOptions>, ConfigureJwtBearerOptions>();

            services.AddScoped<UserService>();
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MyProject.Api", Version = "v1" });

                var securityDefinitionId = "custom jwt auth";

                var securityDefinition = new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Request Header Example: \"Authorization: Bearer {token}\".\nJust insert your JWT token below",
                    Name = securityDefinitionId,
                    In = ParameterLocation.Header,
                    Scheme = "bearer",
                    Type = SecuritySchemeType.Http,
                };

                OpenApiSecurityScheme securityScheme = new OpenApiSecurityScheme()
                {
                    Reference = new OpenApiReference()
                    {
                        Id = securityDefinitionId,
                        Type = ReferenceType.SecurityScheme
                    }
                };

                var securityRequirements = new OpenApiSecurityRequirement()
                {
                    {securityScheme, new string[] { }},
                };

                c.AddSecurityDefinition(securityDefinitionId, securityDefinition);
                c.AddSecurityRequirement(securityRequirements);

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyProject.Api v1"));
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
