﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyProject.Api.Config.Auth;
using MyProject.Api.Models.Args.Users;
using MyProject.Api.Models.Dtos;
using MyProject.Api.Models.Users.Args;
using MyProject.Domain.Models;
using MyProject.Services;
using System.Linq;
using System.Security.Claims;

namespace MyProject.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {

        public UsersController(UserService userService, JwtGenerator jwtGenerator, IMapper mapper, ILogger<UsersController> logger)
        {
            _userService = userService;
            _jwtGenerator = jwtGenerator;
            _mapper = mapper;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost(nameof(Register))]
        public IActionResult Register(RegisterArgs args)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = _mapper.Map<User>(args);

            int newUserId = _userService.CreateUser(user);

            if (newUserId == -1)
                return BadRequest("User cannot be created");

            return Ok(newUserId);
        }

        [AllowAnonymous]
        [HttpPost(nameof(Login))]
        public IActionResult Login(LoginArgs args)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string login = args.Login;
            string passwordHash = args.PasswordHash;

            User[] users = _userService.GetUsersByFilter(user => user.Login == login);

            if (!users.Any())
            {
                return BadRequest($"Incorrect login '{login}'");
            }

            User currentUser = users.SingleOrDefault(u => u.Login == login && u.PasswordHash == passwordHash);

            if (currentUser == null)
            {
                // more than 1 user with same name and password
                return BadRequest("Unexpected error");
            }

            string jwtToken = _jwtGenerator.CreateUserAuthToken(currentUser);
            return Ok(new { jwtToken });
        }

        [Authorize]
        [HttpGet(nameof(GetUsers))]
        public IActionResult GetUsers()
        {
            User[] users = _userService.GetUsers();
            UserDto[] dtoUsers = _mapper.Map<UserDto[]>(users);
            return Ok(dtoUsers);
        }

        [Authorize]
        [HttpPost(nameof(RefreshToken))]
        public IActionResult RefreshToken()
        {
            ClaimsPrincipal requestUser = this.Request.HttpContext.User;
            Claim sidClaim = requestUser.Claims.First(x => x.Type.Equals(ClaimTypes.Sid));
            string currentUserLogin = sidClaim.Value;

            User currentUser = _userService
                .GetUsersByFilter(u => u.Login == currentUserLogin)
                .First();

            string jwtToken = _jwtGenerator.CreateUserAuthToken(currentUser);
            return Ok(new { jwtToken });
        }

        [Authorize]
        [HttpPost(nameof(UpdateUser))]
        public IActionResult UpdateUser(UpdateUserArgs args)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ClaimsPrincipal currentUser = this.Request.HttpContext.User;
            Claim sidClaim = currentUser.Claims.First(x => x.Type.Equals(ClaimTypes.Sid));
            string currentUserLogin = sidClaim.Value;

            User user = _userService
                .GetUsersByFilter(u => u.Login == currentUserLogin)
                .First();

            // todo change the way applying delta 
            if (!string.IsNullOrWhiteSpace(args.Email))
            {
                user.Email = args.Email;
            }

            if (!string.IsNullOrWhiteSpace(args.FirstName))
            {
                user.FirstName = args.FirstName;
            }

            if (!string.IsNullOrWhiteSpace(args.LastName))
            {
                user.LastName = args.LastName;
            }

            _userService.UpdateUser(user.Id, user);
            return Ok();
        }

        [Authorize]
        [HttpPost(nameof(DeleteUser))]
        public IActionResult DeleteUser(DeleteUserArgs args)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int userId = args.UserId;
            _userService.DeleteUser(userId);
            return Ok(userId);
        }

        private readonly UserService _userService;
        private readonly JwtGenerator _jwtGenerator;
        private readonly IMapper _mapper;
        private readonly ILogger<UsersController> _logger;
    }
}
