﻿using System.ComponentModel.DataAnnotations;

namespace MyProject.Api.Models.Args.Users
{
    public sealed class LoginArgs
    {
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string Login { get; set; }
        
        [Required]
        public string PasswordHash{ get; set; }
    }
}
