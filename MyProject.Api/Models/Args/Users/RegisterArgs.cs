﻿using System.ComponentModel.DataAnnotations;

namespace MyProject.Api.Models.Users.Args
{
    public sealed class RegisterArgs
    {
        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string LastName { get; set; }


        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string Login { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string PasswordHash { get; set; }
    }
}
