﻿namespace MyProject.Api.Models.Args.Users
{
    public sealed class UpdateUserArgs
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
