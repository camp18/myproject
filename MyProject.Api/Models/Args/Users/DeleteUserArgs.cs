﻿namespace MyProject.Api.Models.Args.Users
{
    public sealed class DeleteUserArgs
    {
        public int UserId { get; set; }
    }
}
