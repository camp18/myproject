﻿using MyProject.Domain;
using MyProject.Domain.Models;
using System;
using System.Linq;

namespace MyProject.Services
{
    public sealed class UserService
    {
        public UserService(MyAppContext context)
        {
            _context = context;
        }

        public int CreateUser(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return user.Id;
        }

        public User[] GetUsers()
        {
            var users = _context.Users.ToArray();
            return users;
        }

        public User[] GetUsersByFilter(Func<User, bool> filter)
        {
            var users = _context.Users.Where(filter).ToArray();
            return users;
        }

        public int UpdateUser(int userId, User user)
        {
            User existingUser = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (existingUser == null)
                return -1;
            existingUser.FirstName = user.FirstName;
            existingUser.LastName = user.LastName;

            _context.Users.Update(existingUser);
            _context.SaveChanges();
            return userId;
        }

        public int DeleteUser(int userId)
        {
            User existingUser = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (existingUser == null)
                return -1;
            _context.Users.Remove(existingUser);
            _context.SaveChanges();
            return userId;
        }

        private readonly MyAppContext _context;
    }
}
